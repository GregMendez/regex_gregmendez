-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 05 avr. 2021 à 21:24
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `php_regex`
--

-- --------------------------------------------------------

--
-- Structure de la table `actors`
--

CREATE TABLE `actors` (
  `idActor` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `idMovie` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `actors`
--

INSERT INTO `actors` (`idActor`, `name`, `idMovie`) VALUES
(13, 'Adil El Arbi', 9),
(12, 'Alexander Ludwig', 9),
(11, 'Vanessa Hudgens', 9),
(10, 'Martin Lawrence', 9),
(9, 'Will Smith', 9),
(8, 'Bad Boys for Life', 9),
(14, 'Bilall Fallah', 9),
(15, 'Peter Craig', 9),
(16, 'Joe Carnahan', 9),
(17, 'Chris Bremner', 9),
(18, 'Peter Craig', 9),
(19, 'Joe Carnahan', 9),
(20, 'George Gallo', 9),
(21, 'Bad Boys', 10),
(22, 'Sean Penn', 10),
(23, 'Reni Santoni', 10),
(24, 'Jim Moody', 10),
(25, 'Eric Gurry', 10),
(26, 'Rick Rosenthal', 10),
(27, 'Richard Di Lello', 10),
(28, 'sandspider1', 10),
(29, 'American version of Scum', 10),
(30, 'Official Trailer', 10);

-- --------------------------------------------------------

--
-- Structure de la table `movies`
--

CREATE TABLE `movies` (
  `idMovie` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `year` int(4) NOT NULL,
  `notation` double NOT NULL,
  `commentaire` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `movies`
--

INSERT INTO `movies` (`idMovie`, `title`, `image`, `year`, `notation`, `commentaire`) VALUES
(10, 'Bad Boys ', NULL, 1983, 7.2, NULL),
(9, 'Bad Boys for Life ', NULL, 2020, 6.6, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`idActor`);

--
-- Index pour la table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`idMovie`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `actors`
--
ALTER TABLE `actors`
  MODIFY `idActor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `movies`
--
ALTER TABLE `movies`
  MODIFY `idMovie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
