<?php
include 'function.php';

  $source = 'https://www.imdb.com/';
  if(isset($_POST['search']))
  {
    $title = $_POST['search'];
    $source = $source.'find?q='.$title.'&s=tt&ttype=ft&ref_=fn_al_tt_mr';

    $source = file_get_contents($source);

    preg_match_all('/<a href="\/title\/(.{26})" >(.+?)(?=<\/a>)/', $source, $match);
  }

  $movies = getMovies();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <link rel="stylesheet" href="./css/master.css">
    <title></title>
  </head>
  <body>
    <div class="gregContainer">


      <section class="research">
        <h1 class="">Search a movie on IMDB</h1>
        <form action="#" method="post">
          <div class="mb-3">
            <label for="search" class="form-label">Research</label>
            <input type="text" class="form-control" id="search" name="search" aria-describedby="Research">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </section>
      <section class="">
        <?php
        //Affichage des résultats de la recherche
        if(isset($_POST['search']))
        {
          for ($i=0; $i < 10; $i+=2) {
            echo '<div class="item"><div class="">';
            echo $match[2][$i];
            echo '<h5 class="">'. $match[2][$i+1]. '</h5>';
            echo '<button class="btn btn-primary"><a class="button-link" href="./showMovie.php?id='. $match[1][$i].'">More</a></button>';
            echo '</div></div>';
          }
        }
          ?>
      </section>
      <?php

      //Affichage des films présent dans la base de donéées
        if(count($movies) > 1)
        {
          echo '<section>';
          echo '<h2 class="onDb">Movies on database</h2>';


          for ($i=0; $i < count($movies); $i++) {
            echo '<div class="item">';
            echo '<h5>'.$movies[$i]["title"].'</h5>';
            echo '<h6>'.$movies[$i]["year"].'</h6>';
            echo '</div>';
          }
          echo '</section>';
        }

       ?>
    </div>

  </body>
</html>
