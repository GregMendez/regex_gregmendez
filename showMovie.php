<?php
include 'function.php';
if(isset($_GET['id']))
{
  $id = $_GET['id'];
  $source = 'https://www.imdb.com/title/'.$id;

  $source = file_get_contents($source);

  preg_match('/<title>(.*([.\w:]+?)\s)*\((\d*)\)/', $source, $nameAndYear);

  preg_match('/span\s.*ratingValue.[\s]*[\^>]*>(.+?(?=<))/', $source, $note); 
  preg_match_all('/"name": "([\s"\w@:-]*)"/', $source, $match);
  preg_match('/<div class="poster">\n.*\n(.*\n.*)\n<\/a>/', $source, $poster);

  $title = $nameAndYear[1];
  $year = $nameAndYear[3];
  $notation = $note[1];
  $actors = $match[1];

  if(isset($_GET['getMovie']))
  {
    insertMovie($title, $year, $notation, $actors);

  }

}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <link rel="stylesheet" href="./css/master.css">
    <title></title>
  </head>
  <body>
    <div class="gregContainer">
      <h2><?php echo $title ?></h2>
      <div class="">Date de sortie : <?php echo $year ?></div>
      <div class="">Note : <?php echo $notation ?></div>
      <?php echo $poster[0] ?>
      <div class="">
        <?php
          if(count($actors) > 0) echo '<h3>Acteurs :</h3>';
          for ($i=0; $i < count($actors); $i++) {


            echo '<div class="item"><div class="">';
            echo $actors[$i];
            echo '</div></div>';

          }
        ?>
      </div>
      <?php
        echo '<div class="sauvegarde"><button class="btn btn-primary"><a class="button-link" href="showMovie.php?id='.$id.'&getMovie=Yes">Get it</a></button></div>'
      ?>

    </div>
  </body>
</html>
