<?php

function dbConnection() {
  $db = new PDO(
      'mysql:host=localhost;dbname=php_regex',
      'root', '',
      [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
  );
  return $db;
}

function insertMovie($name, $year, $notation, $actors) {

  $db = dbConnection();

  $response = $db->prepare(
      'INSERT INTO movies SET title = :name, year = :year, notation = :notation',
      [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]
  );

  $response->execute([
    ':name' => $name,
    //':image' => $img,
    ':year' => $year,
    ':notation' => $notation
  ]);

  $idMovie = $db->lastInsertId();

  if($response)
  {
    for ($i=0; $i < count($actors); $i++) {
      insertActors($idMovie, $actors[$i]);
    }
  }

}

function insertActors($idMovie, $actor)
{
  $db = dbConnection();

  $response = $db->prepare(
      'INSERT INTO actors SET name = :actor, idMovie = :idMovie',
      [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]
  );

  $response->execute([
    ':actor' => $actor,
    ':idMovie' => $idMovie
  ]);
}

function getMovies()
{

  $db = dbConnection();

  $response = $db->prepare(
      'SELECT * FROM movies ',
      [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]
  );

  $response->execute();

  return $response->fetchAll(PDO::FETCH_ASSOC);
}
